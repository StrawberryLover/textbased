module.exports = function(grunt) {
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      concat: {
        options: {
          separator: '\n'
        },
        basic: {
          src: ['app/src/**/*.js'],
          dest: 'dist/Main.js',
        }
      },
      less: {
        development: {
          options: {
            paths: ['app/less/**/*.less'],
            yuicompress: true,
            optimization: 2
          },
          files: {
            // target.css file: source.less file
            'dist/Main.css': 'app/less/main.less'
          }
        }
      },
      uglify: {
        options: {
          banner: '/*! Made on <%= grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        basic: {
          files: {
            'dist/Main.min.js': ['dist/Main.js']
          }
        },
      },
      jshint: {
        files: ['Gruntfile.js', 'app/src/**/*.js', 'test/*.spec.js'],
        options: {
          // options here to override JSHint defaults
          globals: {
            jQuery: true,
            console: true,
            module: true,
            document: true
          }
        }
      },
      watch: {
        js: {
          files: ['app/src/**/*.js'],
          tasks: ['concat:basic'],
          options: {
            livereload: true,
          }
        },
        css: {
          files: ['app/less/*.less'],
          tasks: ['less'],
          options: {
            nospawn: true,
            livereload: true
          }
        }
      },
      shell: {
        pythonServer: {
          options: {
            stdout: true
          },
          command: 'python -m SimpleHTTPServer 8091'
        }
      },
      concurrent: {
        target: {
          tasks: ['watch', 'shell'],
          options: {
            logConcurrentOutput: true
          }
        }
      }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-concurrent');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'less', 'concurrent:target']);
};

