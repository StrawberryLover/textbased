app.filter('nospace', function () {
	return function (value) {
		if(value)
			return  value.replace(' ', '');
		else
			return value;
	};
});
