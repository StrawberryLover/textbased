var app = angular.module("Hunted", ['ngRoute', 'ui.bootstrap']);

app.config(["$routeProvider", "$locationProvider", "$httpProvider", function($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider.when("/", {
        templateUrl: "app/templates/Main.html",
        controller: "GameController"
    });
}]);