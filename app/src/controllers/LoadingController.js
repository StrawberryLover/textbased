app.controller('LoadingController', ['$scope', '$modalInstance', 'socket', 'BioService', 'items', function($scope, $modalInstance, socket, BioService, item) {
	$scope.item = item;

	$scope.ok = function (item) {
		$modalInstance.close(item);
	};

	socket.on('cleaning', function() {
		$(".stat").text("Cleaning...");
	});

	socket.on('planning',  function() {
		$(".stat").text("Planning...");
	});

	socket.on('begin', function() {
		$scope.ok();
	});

	socket.emit("beginWorld");
}]);
