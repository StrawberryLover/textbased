app.controller('ModalController', ['$scope', '$modalInstance', 'items', function($scope, $modalInstance, items) {
	$scope.list = items; $scope.item = items;

	$scope.ok = function (item) {
		$modalInstance.close(item);
	};

	$scope.prettyPrint = function(json) {
		return JSON.stringify(json, undefined, 2);
	};
}]);