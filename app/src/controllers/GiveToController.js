app.controller('GiveToController', ['$scope', '$modalInstance', 'GameState', 'items', '$rootScope', function($scope, $modalInstance, GameState, item, $rootScope) {
	$scope.ok = function (item) {
		$modalInstance.close(item);
	};

	$scope.prettyPrint = function(json) {
		return JSON.stringify(json, undefined, 2);
	};

    $scope.giveTo = function(entity) {
        GameState.toInventory(item, entity.name);
        $scope.ok(item);
    };
}]);
