app.controller('BioController', ['$scope', '$modalInstance', 'BioService', 'items', function($scope, $modalInstance, BioService, item) {
	$scope.item = item.entity;
	$scope.type = item.type;
	$scope.bioInfo = BioService.getBioInfo($scope.item.name);

	$scope.ok = function (item) {
		$modalInstance.close(item);
	};

	$scope.prettyPrint = function(json) {
		return JSON.stringify(json, undefined, 2);
	};
}]);