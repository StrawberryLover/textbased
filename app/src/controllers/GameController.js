app.controller('GameController', ['$scope', '$rootScope', 'modal', 'GameState',  'helper', function($scope, $rootScope, modal, game, helper) {

	// Player sends action to the SAGA system
	$scope.onQuery = function() {
		$scope.console.push($scope.query);
	};

	// Toggle Game state
	$scope.toggleGame = function() {
		if($scope.state == "play")
			game.start();
		else
			game.stop();

		$scope.state = ($scope.state == "play")?"pause":"play";
	};

	$scope.characters = function() {
		modal.open("lgx", "CharactersModal", null, $rootScope.world.entities.Character);
	};

	$scope.info = function(entity) {
		modal.open("lg", "infoModal", null, entity);
	};

    $scope.bio = function(entity, type) {
        modal.open("lg", "BioModal", null, { entity: entity, type: type}, 'BioController');
    };

    $scope.openInventory = function(entity) {
        modal.open("lg", "BioModal", null, { entity: entity, type: type}, 'BioController');
    };

    $scope.charBio = function(entity) {
        modal.open("lg", "CharBioModal", null, entity, 'CharBioController');
    };

    $scope.pickup = function(entity) {
        game.toInventory(entity);
    };

    $scope.hasItem = function(entity) {
        return helper.getCharacterInventory(entity);
    };

    $scope.giveTo = function(item) {
        modal.open("sm", "GiveToModal", null, item, 'GiveToController');
    };
}]);
