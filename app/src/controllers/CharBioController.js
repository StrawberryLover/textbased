app.controller('CharBioController', ['$scope', 'helper', '$modalInstance', 'BioService', 'items', function($scope, helper, $modalInstance, BioService, item) {
	$scope.item = item;
	$scope.bioInfo = BioService.getBioInfo(item.name);
    $scope.charInventory = helper.getCharacterInventory(item.name);

	$scope.ok = function (item) {
		$modalInstance.close(item);
	};

	$scope.prettyPrint = function(json) {
		return JSON.stringify(json, undefined, 2);
	};
}]);
