app.directive('card', function ($rootScope) {
	return {
		restrict: 'AE',
		templateUrl : 'app/templates/card.html',
		scope: {
			action: '=effect',
		},
		link: function(scope, elem, attrs) {
			console.log("Created: " + scope.action.title);

			elem.hover(function() {
				if(scope.action.effects.length > 2) {
					elem.find(".flip-container").css("min-height", 90 + 20 * scope.action.effects.length + "px");
				}
			}, function() {
				elem.find(".flip-container").css("min-height", "110px");
			});
		}
	};
});

