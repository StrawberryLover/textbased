app.factory('GameSocket',  ['$rootScope', 'modal', 'socket', 'StateComparator', function ($rootScope, modal, socket, state) {
    // Connect
    socket.on('connect', function() { state.onConnect(); });

    // Disconnect
    socket.on('disconnect', function() { state.onDisconnect(); });

    // On World List
    socket.on('onWorldList', function(list) {
        modal.open("lg", "ListModal", function (worldName) {
            socket.emit('createWorld', worldName, function(blueprints) { 
                state.init(worldName, blueprints); 
            });
        }, list);
    });

    // Update from the SAGA system
    socket.on('action', function(action) {
        state.onEffects(action);
    });

    return {
        connect : function() {
            console.log("Connect");
        },
        start : function() {
            //socket.emit('beginWorld');
        },
        stop : function() {
            socket.emit('endWorld');
        },
        action : function(state) {
            socket.emit('onAction', state, function(response) {
                console.log(response);
            });
        }
    };
}]);