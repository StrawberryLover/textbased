app.factory('StateComparator', ['$rootScope', 'GameConverter', 'GameState', 'SagaState', function ($rootScope, gameConverter, game, saga) {
	return {
		onConnect : function() {
			game.onConnect();
		},
		onDisconnect : function() {
			game.onDisconnect();
		},
		onGame : function() {
			
		},
		init : function(worldName, world) {
			// Game world, init world
			gameConverter.init(worldName, world);

			// SAGA world, init world
			saga.init(world);
		},
		onEffects : function(action) {
			// Update game state
			gameConverter.onEffect(action);

			// Update saga state
			saga.onEffect(action);
		}
	};
}]);