app.factory('BioService', function() {
    var info = {
        "palace": {
            "bioText": {
                "paragraphs": [
                    "The word 'palace' comes from Old French palais (imperial residence), from Latin Palātium, the name of one of the seven hills of Rome. The original 'palaces' on the Palatine Hill were the seat of the imperial power while the 'capitol' on the Capitoline Hill was the religious nucleus of Rome. Long after the city grew to the seven hills the Palatine remained a desirable residential area. Emperor Caesar Augustus lived there in a purposely modest house only set apart from his neighbours by the two laurel trees planted to flank the front door as a sign of triumph granted by the Senate. His descendants, especially Nero, with his 'Golden House' enlarged the house and grounds over and over until it took up the hill top. The word Palātium came to mean the residence of the emperor rather than the neighbourhood on top of the hill."
                ]
            },
            "extraInfo": {
                "listItems": [
                    { "title": "Location", "text": "Sacre Blu, France" },
                    { "title": "Size", "text": "400m"},
                    { "title": "Furnishings", "text": "Post-baroque neo-gothic"}
                ]
            }
        },
        "fountain": {
            "bioText": {
                "paragraphs": [
                    "Fountains were originally purely functional, connected to springs or aqueducts and used to provide drinking water and water for bathing and washing to the residents of cities, towns and villages. Until the late 19th century most fountains operated by gravity, and needed a source of water higher than the fountain, such as a reservoir or aqueduct, to make the water flow or jet into the air.", 
                    "In addition to providing drinking water, fountains were used for decoration and to celebrate their builders. Roman fountains were decorated with bronze or stone masks of animals or heroes. In the Middle Ages, Moorish and Muslim garden designers used fountains to create miniature versions of the gardens of paradise. King Louis XIV of France used fountains in the Gardens of Versailles to illustrate his power over nature. The baroque decorative fountains of Rome in the 17th and 18th centuries marked the arrival point of restored Roman aqueducts and glorified the Popes who built them." 
                ]
            },
            "extraInfo": {
            }
        },
        "tower": {
            "bioText": {
                "paragraphs": [
                    "Towers have been used by mankind since prehistoric times. The oldest known may be the circular stone tower in walls of Neolithic Jericho (8000 BC). Some of the earliest towers were ziggurats, which existed in Sumerian architecture since the 4th millennium BC. The most famous ziggurats include the Sumerian Ziggurat of Ur, built the 3rd millennium BC, and the Etemenanki, one of the most famous examples of Babylonian architecture. The latter was built in Babylon during the 2nd millennium BC and was considered the tallest tower of the ancient world.", 
                    "Some of the earliest surviving examples are the broch structures in northern Scotland, which are conical towerhouses. These and other examples from Phoenician and Roman cultures emphasised the use of a tower in fortification and sentinel roles. For example, watchtower elements are found at Mogador from the first millennium BC, derived from Phoenician or Carthaginian origins. The Romans utilised octagonal towers as elements of Diocletian's Palace in Croatia, which monument dates to approximately 300 AD, while the Servian Walls (4th century BC) and the Aurelian Walls (3rd century AD) featured square ones. The Chinese used towers as integrated elements of the Great Wall of China in 210 BC during the Qin Dynasty. Towers were also an important element of castles."
                ]
            },
            "extraInfo": {
            }
        },
        "road": {
            "bioText": {
                "paragraphs": [
                    "Road construction requires the creation of a continuous right-of-way, overcoming geographic obstacles and having grades low enough to permit vehicle or foot travel and may be required to meet standards set by law or official guidelines. The process is often begun with the removal of earth and rock by digging or blasting, construction of embankments, bridges and tunnels, and removal of vegetation (this may involve deforestation) and followed by the laying of pavement material. A variety of road building equipment is employed in road building.",
                    "After design, approval, planning, legal and environmental considerations have been addressed alignment of the road is set out by a surveyor. The radii and gradient are designed and staked out to best suit the natural ground levels and minimize the amount of cut and fill. Great care is taken to preserve reference Benchmarks."
                ]
            },
            "extraInfo": {
            }
        },
        "garden": {
            "bioText": {
                "paragraphs": [
                    "A garden is a planned space, usually outdoors, set aside for the display, cultivation, and enjoyment of plants and other forms of nature. The garden can incorporate both natural and man-made materials. The most common form today is known as a residential garden, but the term garden has traditionally been a more general one. Zoos, which display wild animals in simulated natural habitats, were formerly called zoological gardens. Western gardens are almost universally based on plants, with garden often signifying a shortened form of botanical garden.", 
                    "Some traditional types of eastern gardens, such as Zen gardens, use plants sparsely or not at all. Xeriscape gardens use local native plants that do not require irrigation or extensive use of other resources while still providing the benefits of a garden environment. Gardens may exhibit structural enhancements, sometimes called follies, including water features such as fountains, ponds (with or without fish), waterfalls or creeks, dry creek beds, statuary, arbors, trellises and more."
                ]
            },
            "extraInfo": {
            }
        },
        "tlc": {
            "bioText": {
                "paragraphs": []
            },
            "extraInfo": {
            }
        },
        "goldball": {
            "bioText": {
                "paragraphs": [
                    "Gold balls are things that princesses are particularly drawn to. Wizards and scientists all over the world believe there is some hidden curse element inside all gold balls. If a princess comes into close proximity of a gold ball various symptoms arise in the princess.",
                    "The most serious symptoms include: a sudden perversion toward large slimy toads, anxiety, a desire to trust all witches that the patient comes into contact with and a inclination toward taking long naps after getting stung by needles."
                ]
            },
            "extraInfo": {
            }
        },
        "radish": {
            "bioText": {
                "paragraphs": [
                    "Varieties of radish are now broadly distributed around the world, but there are almost no archeological records available to help determine its early history and domestication.However, scientists tentatively locate the origin of Raphanus sativus in southeast Asia, as this is the only region where truly wild forms have been discovered. India, central China, and central Asia appear to have been secondary centers where differing forms were developed. Radishes enter the historical record in 3rd century b.c. Greek and Roman agriculturalists of the 1st century a.d. gave details of small, large, round, long, mild, and sharp varieties. The radish seems to have been one of the first European crops introduced to the Americas. A German botanist, reported radishes of 100 pounds (45 kg) and roughly three feet in length in 1544, although the only variety of that size today is the Japanese Sakurajima radish. The large, mild, and white East Asian form was developed in China but is mostly associated in the West with the Japanese daikon, owing to Japanese agricultural development and larger exports."
                ]
            },
            "extraInfo": {
            }
        },
        "princess": {
            "bioText": {
                "paragraphs": ["The princess is an avatar of tranquility and peace. However she has gullible tendencies and is too trustworthy of old, long nosed ladies in black robes.",
                                "The titles of some princesses hold their titles are reigning monarchs of principalities. There have been fewer instances of reigning princesses than reigning princes as most principalities excluded women from inheriting the throne. Examples of princesses regnant have included Constance of Antioch, princess regnant of Antioch in the 12th century. As the President of France, an office for which women are eligible, is ex-officio co-Prince of Andorra, Andorra could theoretically be ruled by a co-Princess."
                ]
            },
            "extraInfo": {
            }
        },
        "frog": {
            "bioText": {
                "paragraphs": ["Frogs are a diverse and largely carnivorous group of short-bodied, tailless amphibians composing the order Anura (Ancient Greek an-, without + oura, tail). The oldest fossil 'proto-frog' appeared in the early Triassic of Madagascar, but molecular clock dating suggests their origins may extend further back to the Permian, 265 million years ago. Frogs are widely distributed, ranging from the tropics to subarctic regions, but the greatest concentration of species diversity is found in tropical rainforests. There are approximately 4,800 recorded species, accounting for over 85% of extant amphibian species. They are also one of the five most diverse vertebrate orders."]
            },
            "extraInfo": {
            }
        },
        "king": {
            "bioText": {
                "paragraphs": ["A monarch is the sovereign head of state, officially outranking all other individuals in the realm. A monarch may exercise the most and highest authority in the state or others may wield that power on behalf of the monarch. Typically a monarch either personally inherits the lawful right to exercise the state's sovereign rights (often referred to as the throne or the crown) or is selected by an established process from a family or cohort eligible to provide the nation's monarch. Alternatively, an individual may become monarch by conquest, acclamation or a combination of means. A monarch usually reigns for life or until abdication. Monarchs' actual powers vary from one monarchy to another and in different eras; on one extreme, they may be autocrats (absolute monarchy) wielding genuine sovereignty; on the other they may be ceremonial heads of state who exercise little or no power or only reserve powers, with actual authority vested in a parliament or other body (constitutional monarchy).",
                "Taken from Wikipedia."]
            },
            "extraInfo": {
            }
        },
        "frogservant": {
            "bioText": {
                "paragraphs": ["Frog servants are much like regular frogs. They bear the same charectaristics as them and they taste like them too. What differentiates a frog servant from regular frogs though is that they can talk, dance and write snappy poetry. This is in large part thanks to witch magic. Only a witch can turn a regular frog into a frog servant. This comes with a disadvantage though, the frog servant is forced to serve the witch until his death."]
            },
            "extraInfo": {
            }
        },
        "man": {
            "bioText": {
                "paragraphs": ["A man is a male human. The term man is usually reserved for an adult male, with the term boy being the usual term for a male child or adolescent. However, the term man is also sometimes used to identify a male human, regardless of age, as in phrases such as 'men's basketball'.",
                "Like most other male mammals, a man's genome typically inherits an X chromosome from his mother and a Y chromosome from his father. The male fetus produces larger amounts of androgens and smaller amounts of estrogens than a female fetus. This difference in the relative amounts of these sex steroids is largely responsible for the physiological differences that distinguish men from women. During puberty, hormones which stimulate androgen production result in the development of secondary sexual characteristics, thus exhibiting greater differences between the sexes. However, there are exceptions to the above for some intersex and transgender men.",
                "Taken from Wikipedia."]
            },
            "extraInfo": {
            }
        },
        "wife": {
            "bioText": {
                "paragraphs": ["A wife is a female partner in a continuing marital relationship. A wife may also be referred to as a spouse. The term continues to be applied to a woman who has separated from her partner and ceases to be applied to such a woman only when her marriage has come to an end following a legally recognised divorce or the death of her spouse. On the death of her partner, a wife is referred to as a widow, but not after she is divorced from her partner.",
                                "The rights and obligations of the wife in relation to her partner and her status in the community and in law varies between cultures and has varied over time.",
                                "Taken from Wikipedia."]
            },
            "extraInfo": {
            }
        },
        "witch": {
            "bioText": {
                "paragraphs": ["Witchcraft (also called witchery or spellcraft) broadly means the practice of, and belief in, magical skills and abilities that are able to be exercised individually, by designated social groups, or by persons with the necessary esoteric secret knowledge. Witchcraft is a complex concept that varies culturally and societally, therefore it is difficult to define with precision and cross-cultural assumptions about the meaning or significance of the term should be applied with caution. Witchcraft often occupies a religious, divinatory, or medicinal role, and is often present within societies and groups whose cultural framework includes a magical world view. Although witchcraft can often share common ground with related concepts such as sorcery, the paranormal, magic, superstition, necromancy, possession, shamanism, healing, spiritualism, nature worship, and the occult, it is usually seen as distinct from these when examined by sociologists and anthropologists."]
            },
            "extraInfo": {
            }
        },
        "rapunzel": {
            "bioText": {
                "paragraphs": ["Rapunzel is a German fairy tale in the collection assembled by the Brothers Grimm, and first published in 1812 as part of Children's and Household Tales. The Grimm Brothers' story is an adaptation of the fairy tale Rapunzel by Friedrich Schulz published in 1790. The Schulz version is based on Persinette by Charlotte-Rose de Caumont de La Force originally published in 1698. Its plot has been used and parodied in various media and its best known line ('Rapunzel, Rapunzel, let down your hair') is an idiom of popular culture.",
                "Taken from Wikipedia."]
            },
            "extraInfo": {
            }
        },
        "kingson": {
            "bioText": {
                "paragraphs": ["A prince is a male ruler, monarch, or member of a monarch's or former monarch's family. Prince is a hereditary title in the nobility of some European states. The feminine equivalent is a princess. The English word derives, via the French word prince, from the Latin noun princeps, from primus (first) + capio (to seize), meaning 'the chief, most distinguished, ruler, prince'.",
                "Taken from Wikipedia."]
            },
            "extraInfo": {
            }
        },
        "emptytemplate": {
            "bioText": {
                "paragraphs": []
            },
            "extraInfo": {
            }
        }
    };

    return  {
        getBioInfo: function(name) {
            return info[name.toLowerCase().replace(/ /g, '')];
        }
    };
});
