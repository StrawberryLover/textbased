app.factory('timer', function ($rootScope, $interval) {
	var registrants = {}, interval = 1000 / 60;

	var start = function() {
		$interval(service.tick, interval);
		service.tick();
	};

	var service =  {
		register: function(cardId, card) {
			registrants[cardId] = {
				id : cardId,
				idCSS : "#card" + cardId,
				title : card.title,
				cb : card.callback,
				startTime : card.startTime,
				time : card.time,
				tick: function() {
					// Calc percentage
					this.pct = ((new Date().getTime() - this.startTime) / this.time) * 100;

					// Update the progress bar
					$(this.idCSS).find(".progress-bar").css("width", Math.round(this.pct * 10) / 10 + "%");
					$(this.idCSS).find(".info-card-ptc").text(Math.round(this.pct) + "%");

					// Check the limit
					if(this.pct > 100) {
						// Stop the countdown
						service.unregister(this.id);
						
						// Let the queue know
						this.cb(this.id, this.title);
					}
				}
			};
		},
		unregister: function(id) {
			delete registrants[id];
		},
		tick: function() {
			angular.forEach(registrants, function(reg) {
				reg.tick();
			});
		}
	};

	start();

	return service;
});