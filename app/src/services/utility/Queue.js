app.factory('queue', function ($rootScope, $injector, timer) {
    $rootScope.queue = [];
    $rootScope.console = [];
    var socket;

    function parse(string) {
        string = string.toLowerCase().replace("_", " ");

        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getTitle(card) {
        switch(card.name) 
        {
            case 'MOVE':
                return parse(card.entities[0].name + " moves to " + card.entities[2].name); 
            case 'LOSE':
                return parse(card.entities[0].name + " lost " + card.entities[1].name);
            case 'GIVE':
                return parse(card.entities[0].name + " gives " + card.entities[1].name + " a " + card.entities[2].name);
            case 'FIND':
                return parse(card.entities[0].name + " finds " + card.entities[1].name);
            default:
                console.warn("Ignored " + card.name);
        }

        return "Unknown Action";
    }

    return {
        connected : function() {
            if (!socket) { socket = $injector.get('GameSocket'); }

            /*$rootScope.queue.push({
                name : "Connected",
                status : "on"
            });*/
        },
        disconnected : function() {
            /*$rootScope.log = [];
            
            $rootScope.log = [{
                name : "Disconnected",
                status : "off"
            }];*/
        },
        world : function() {
            var world = $rootScope.world, relations = world.relations;

            // Relations
            for (var i = 0; i < relations.length; i++) {
                rel = relations[i];
                $rootScope.log.push(rel);
            }

            // Entities
            var entities = world.entities;

            for(var type in entities) {
                for(i = 0; i < entities[type].length; i++) {
                    entity = entities[type][i];
                    $rootScope.log.push(entity);
                }
            }
        },
        effect : function(card) {
            card.time = 30000 * ($rootScope.queue.length + 1);
            card.title = getTitle(card);
            card.startTime = new Date().getTime();
            card.pct = ((new Date().getTime() - card.startTime) / card.time) * 100;

            $rootScope.queue.push(card);
            timer.register(card.id, card);
        },
        execute : function(action, success) {
            socket.action({ id : action, execute : success });
        }
    };
});
