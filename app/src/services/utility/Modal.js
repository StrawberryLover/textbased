app.factory('modal', function ($rootScope, $modal) {
	return {
		open: function (size, template, callback, items, controller, classModal) {
			var logic = (controller) ? controller : 'ModalController';
			classModal = (classModal) ? classModal : 'modal';

			var modalInstance = $modal.open({
				templateUrl: 'app/templates/modals/' + template + '.html',
				controller: logic,
				size: size,
				windowClass : classModal,
				resolve: {
					items: function () {
						return items;
					}
				}
			});

			modalInstance.result.then(callback);
		}
	};
});
