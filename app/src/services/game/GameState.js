app.factory('GameState', function ($rootScope, $injector, modal, queue, helper, GameHandler) {
	var socket;

	return {
		onConnect: function() {
			$rootScope.world = {
                player : {
                    inventory : []
                },
				entities : {},
				relations : [],
				locations : [],
				items : []
			};

			queue.connected();

			if (!socket) { socket = $injector.get('GameSocket'); }
		},
		onDisconnect: function() {
			$rootScope.world = {};
			queue.disconnected();
		},
		init : function(world) {
			$rootScope.world = world;

			this.parseEffect($rootScope.world.relations);
			modal.open("sm", "LoadingModal", null, null, "LoadingController", "loading");
		},
		start : function() {
			socket.start();
		},
		stop : function() {
			socket.stop();
		},
		parseEffect : function(effects) {
			var effect, success = true;

			for(var i = 0; i < effects.length; i++) {
				effect = effects[i];

				switch(effect.activity) {
					case "IS_AT":
						success = GameHandler.toLocation(effect);
						break;
					case "HAS":
						success = (effect.operator == "REMOVE") ? GameHandler.removeItem(effect) : GameHandler.addItem(effect);
						break;
					case "OWNS":
						success = (effect.operator == "REMOVE") ? GameHandler.removeItem(effect) : GameHandler.addItem(effect);
						break;
					case "LOST":
						success = GameHandler.removeItem(effect);
						break;
					case "EXPRESSION":
						success = GameHandler.removeItem(effect);
						break;
					case "EDIBLE":
						success = GameHandler.setItemStat(effect);
						break;
					default:
						console.warn("Unknown activity: "  + effect.activity);					
				}

				// Stop on unsuccessful effect
				if(!success) {
					console.warn("Unsuccessful effect " + effect.activity);
					break;
				}
			}

			return success;
		},
		onEffect : function(action) {
			var that = this;	// wrap object scope

			queue.effect({
				id : action.id,
				name : action.name,
				entities : action.entities,
				effects : action.effects,
				callback : function(id, name) {
					$rootScope.console.push(name);

					if(that.parseEffect(action.effects)) 
						queue.execute(action.id, true); 
					else {
						queue.execute(action.id, false); 
						console.warn("Faild to execute " + action.effects);
					}

					/// Remove the card from queue
					for(var i = 0; i < $rootScope.queue.length; i++) {
						if($rootScope.queue[i].id == id) {
							$rootScope.queue.splice(i, 1);
						}
					}
				}
			});
		},
        toInventory : function(item, newOwner) {
            var stuff = helper.item($rootScope.world.items, item.name);
            if (newOwner === undefined) {
                newOwner = "You";
            }
            if (!stuff) {
                helper.remove($rootScope.world.player.inventory, item);
                item.owner = newOwner;
                $rootScope.world.items.push(item);
            }
            else {
                helper.remove($rootScope.world.items, item);
                item.owner = newOwner;
                $rootScope.world.player.inventory.push(item);
            }
        }
	};
});
