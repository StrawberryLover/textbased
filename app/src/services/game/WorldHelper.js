app.factory('helper', function ($rootScope) {
	return {
		contains: function(list, item, property) {
			var next;

			for(var i = 0; i < list.length; i++) {
				next = (property) ? list[i][property] : list[i];

				if(next == item)
					return i;
			}

			return -1;
		},
		exists: function(list, item, property) {
			var next;

			for(var i = 0; i < list.length; i++) {
				next = (property) ? list[i][property] : list[i];

				if(next == item)
					return true;
			}

			return false;
		},
		remove: function(list, item) {
			var index = this.contains(list, item);

			if(index >= 0)
				list.splice(index, 1);

			return index >= 0;
		},
		removeFromLocation : function(name) {
			for(var i = 0; i < $rootScope.world.locations.length; i++) {
				if(this.remove($rootScope.world.locations[i], name))
					return true;
			}

			return false;
		},
		character : function(name) {
			var characters = $rootScope.world.entities.Character;

			for(var i = 0; i < characters.length; i++) {
				if(characters[i].name == name) {
					return characters[i];
				}
			}

			return null;
		},
		getCharacterInventory : function(charName) {
			index = this.contains($rootScope.world.items, charName, "owner");

			if(index >= 0)
				return $rootScope.world.items[index];

			return false;
		},
		item : function(list, name) {
			var index = this.contains(list, name, "name");

			if(index >= 0)
				return list[index];

			return false;
		},
		location : function(name, to) {
			var index = this.contains($rootScope.world.locations, name, "name");

			if(index >= 0)
				return $rootScope.world.locations[index];

			return false;
		},
		pretty : function(name) {
			name = name.replace(new RegExp("_", 'g'), " ");
			return name.charAt(0) + name.toLowerCase().slice(1);
		}
	};
});
