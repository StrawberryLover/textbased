app.factory('GameHandler', function ($rootScope, helper) {
	return {
		addItem: function(effect) {
			var name, item, character;

			// Set item name
			name = helper.pretty(effect.entity2);
			item = helper.item($rootScope.world.items, name);

			// Add to Characters inventory
			if(((character = helper.character(effect.character)) !== null)) {
				if(!character.items)
					character.items = [];

				// If item does not exist in inventory we add it
				if(!helper.exists(character.items, name))
					character.items.push(name);

				// Update item
				item.owner = character.name;
				item.location = character.location;
			} else {
				// Character who owns item does not exist
				return false;
			}

			// Add to World inventory
			if(!helper.exists($rootScope.world.items, name, "name")) {
				$rootScope.world.items.push({
					name : name,
					location : character.location,
					owner : character.name
				});
			}

			return true;
		},
		removeItem: function(effect) {
			var name, item, character;

			// Get Item
			name = helper.pretty(effect.entity2 || effect.entity1);
			item = helper.item($rootScope.world.items, name);

			// Remove from Character
			if(((character = helper.character(effect.character)) !== null))
				if(!helper.remove(character.items, name))
					return false; // Faild to remove 

			item.owner = "unknown";

			// Remove item from its location
			helper.removeFromLocation(name);

			// Effects are not items!
			if(effect.operator == "EXPRESSION")
				return helper.remove($rootScope.world.items, name);

			return true;
		},
		setItemStat : function(effect) {
			var name, item;

			// Get Item
			name = helper.pretty(effect.entity2 || effect.entity1);

			// Add to World inventory
			if(!helper.exists($rootScope.world.items, name, "name")) {
				$rootScope.world.items.push({
					name : name,
					location : "unknown",
					owner : "unknown"
				});
			}


			item = helper.item($rootScope.world.items, name);

			// Item does not exist
			if(!item)
				return false;

			item[effect.name] = true;

			return true;
		},
		toLocation: function(effect) {
			var name, place, entity;

			name = helper.pretty(effect.entity2);

			// Add location to the world
			if(!helper.exists($rootScope.world.locations, name, "name")) {
				$rootScope.world.locations.push({
					name : name,
					characters : []
				});
			}

			// Get location
			place = helper.location(name);

			// Move character
			if((entity = helper.character(effect.character)) !== null)  {
				entity.location = name;
			} else {
				entity = helper.item($rootScope.world.items, effect.character);
			}

			// No character or item found
			if(!entity)
				return false;

			if(effect.operator == "ADD" || typeof effect.operator === 'undefined')
				place.characters.push(entity.name);
			else
				return helper.remove(place.characters, entity.name);

			return true;
		}
	};
});