app.factory('GameConverter', function ($rootScope, GameState, helper) {
	return {
		init : function(worldName, world) {
			var parsed = $rootScope.world;

			parsed.name = helper.pretty(worldName);

			// Parse Entities
			for (var i = 0; i < world.entities.length; i++) {
				entity = world.entities[i];

				if(!parsed.entities[entity.type])
					parsed.entities[entity.type] = [];

				parsed.entities[entity.type].push( {
					name : helper.pretty(entity.name)
				});
			}

			// Parse Relations
			for (i = 0; i < world.relations.length; i++) {
				rel = world.relations[i];
				rel = this.parseEffect(rel);

				parsed.relations.push(rel);
			}

			GameState.init(parsed);
		},
		onEffect : function(action) {
			for (var i = 0; i < action.effects.length; i++) {
				this.parseEffect(action.effects[i]);
			}

			GameState.onEffect(action);
		},
		parseEffect : function(effect) {
			effect.activity = effect.name;
			effect.character = helper.pretty(effect.entity1);
			effect.name = helper.pretty(effect.entity1 + " " + effect.name + " " + effect.entity2);
			return effect;
		}
	};
});
